import 'zone.js/dist/zone-node.js';
import 'reflect-metadata';

import { renderModuleFactory } from '@angular/platform-server';
import { enableProdMode } from '@angular/core';

import * as express from 'express';
import { join } from 'path';
import { readFileSync } from 'fs';

enableProdMode();

const app = express();

const PORT = process.env.PORT || 4000;
const DIST_FOLDER = join(process.cwd(), 'dist');

const template = readFileSync(join(DIST_FOLDER, 'browser', 'index.html')).toString();
const { AppServerModuleNgFactory, LAZY_MODULE_MAP } = require('./dist/server/main.bundle');
const { provideModuleMap } = require('@nguniversal/module-map-ngfactory-loader');
const rootZone = Zone.current;

app.engine('html', (_, options, callback) => {
  renderModuleFactory(AppServerModuleNgFactory, {
    document: template,
    url: options.req.url,
    extraProviders: [
      provideModuleMap(LAZY_MODULE_MAP)
    ]
  }).then(html => {
    callback(null, html);
  });
});

app.set('view engine', 'html');
app.set('views', join(DIST_FOLDER, 'browser'));
app.get('*.*', express.static(join(DIST_FOLDER, 'browser')));
// process.on('uncaughtException', function (err) { console.error('Error: ', err); });
app.get('*', (req, res) => {
  rootZone.fork({
    name: 'Un contreled exception',
    onHandleError: (parentZoneDelegate, currentZone, targetZone, error) => {
      parentZoneDelegate.handleError(targetZone, error);
      res.sendFile('error.html', { root: './src' });
      return false;
    }
  }).run(() => {
    res.render(join(DIST_FOLDER, 'browser', 'index.html'), { req });
  });
});

app.listen(PORT, () => {
  console.log(`Node server listening on http://localhost:${PORT}`);
});
