let merge = require('webpack-merge');
let webpackConfgServer = require ('./webpack.server.config.js');

let webpackConfigDev = {
  watch: true
};

module.exports = merge(webpackConfgServer, webpackConfigDev);
